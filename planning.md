# Django One Shot

## Todo list manager

-each todo list has a name work or home
- each todo list has todo items
- todo items have tasks description, optional due date, and a complete indicator
  
 # Feature 1 - Setup [x]

    -[x] Setup virtual Environment
    -[x] Activate Environment
    -[x] Upgrade Pip
    -[x] Install Requirements
    -[x] Deactivate Environments
    -[x] Activate your Virtual Environment
    -[x] pip freeze > requirements.txt 
    -[x] git

# Feature 2 - brain_two [x]
    
    -[x] Create a repository named brain_two so manage.py file is in top directory
    -[x] Create a Django App named todos and install it in the Brain_two Django Project Installed apps
    -[x] Run Migrations
    -[x] Create a super User
    -[x] Run Server
    -[x] git

# Feature 3 - Todo List Model [x]

    -[x] Todo Model List
        -[x] name - string - max length 100
        -[x] create_one - date/time - should be automatically set to the time the dolist was made
        -[x] convert to string with __str__ method with the name value
        -[x] make migrations
        -[x] git

# Feature 4 - Register Todo List Model on admin [x]

    -[x] Todo List model admin
    -[x] go to site and look at admin panel in local/admin
    -[x] git

# Feature 5 - Todo Item Model [x]

    -[x] TodoItem Model
        -[x] task - string - max length 100
        -[x] due_date - date/time - should be optional
        -[x] is_completed - boolean -  should default false
        -[x] list - foreignkey - should be related to the Todo List Model 
            -[x] related name = items
            -[x] on_delete cascade)

# Feature 6 - Register Todo Item Model on admin [x]

    -[x] TodoItem model admin
        -[x] go to site and look at admin panel in local/admin
        -[x] git

# Feature 7 - List View for Todo List Model [x]

    -[x] Create view that will get all instance of the TodoList model and puts them in the context of the template
    -[x] Register the views in the todos app for the path "" and the name "list_todos" in a new file named todos/urls.py
    -[x] Include the url patterns from todos app in the brain_two project with the prefix "todos/"
    -[x] Create a template for the list view that complies with the following specifications
        * Had to go to views to find out todolist_list is name to loop thru 

# Feature 8 - This feature allows people to see the details about a todo list, including the list of todo items [x]

    -[x] Create a view that shows the details of a particular todo list, including its task
    -[x] In the todos urls.py file, register the view with the path "<int:pk>/" and the name "show_todolist"
    -[x] Create a template to show the details of the todolist and a table of its todo items
    -[x] Update the list template to show the number of todo items for a todo list
    -[x] Update the list template to have a link from the todo list name to the detail view for that todo list

# Feature 9 - This feature allows a person to go from the todo list page to a page that allows them to create a new todo list.

    -[ ] Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList
    -[ ] If the todo list is successfully created, it should redirect to the detail page for that todo list
    -[ ] Register that view for the path "create/" in the todos urls.py and the name "create_todolist"
    -[ ] Create an HTML template that shows the form to create a new TodoList (see the template specifications below)
    -[ ] Add a link to the list view for the TodoList that navigates to the new create view

